const gulp      =       require('gulp');
sass            =       require('gulp-dart-sass');
autoprefiexer   =       require('gulp-autoprefixer');
var browserSync =       require('browser-sync').create();
 
function style() {
    return gulp.src('*.scss')
    .pipe(sass(
        {
            outputStyle: 'compressed'//compressed,
        }
    ).on('error', sass.logError))
    .pipe(autoprefiexer(
        {
            version: [
                'last 2 browsers'
            ]
        }
    ))
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
           baseDir: './'
        },
    });
    gulp.watch('./resource/scss/**/*.scss', style);
    gulp.watch('./**/*.html').on('change', browserSync.reload);
    gulp.watch('./**/*.js').on('change', browserSync.reload);
}

exports.watch = watch;